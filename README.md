## Stay offline

!! Not functional right now !!!

A simple android app that keep the phone in offline mode (or plane mode) as long as possible and
bring it online for short period of time.

Being offline offers several benefits:
- do not be disturbed by other people all the time
- not being traced by government through mobile network triangulation

Bring the phone online from notification in the status bar, at defined hour or through quick settings tile.

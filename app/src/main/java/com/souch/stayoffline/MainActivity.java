/*
 * Stay Offline - keep the phone in offline mode as long as possible
 * Copyright (C) 2021  Mathieu Souchaud
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.souch.stayoffline;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;

import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.nfc.NfcAdapter;
import android.nfc.NfcManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        int color;

        color = getResources().getColor(R.color.disabled);
        if (isAirplaneModeOn(getApplicationContext())) {
            color = getResources().getColor(R.color.enabled);
        }
        findViewById(R.id.textViewStatusPlane).setBackgroundColor(color);

        NetworkInfo mobile = connManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        color = getResources().getColor(R.color.disabled);
        if (mobile.isConnected()) {
            color = getResources().getColor(R.color.enabled);
        }
        findViewById(R.id.textViewStatusMobile).setBackgroundColor(color);

        NetworkInfo wifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        color = getResources().getColor(R.color.disabled);
        if (wifi.isConnected()) {
             color = getResources().getColor(R.color.enabled);
        }
        findViewById(R.id.textViewStatusWifi).setBackgroundColor(color);

        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        color = getResources().getColor(R.color.disabled);
        if (bluetoothAdapter != null &&
                bluetoothAdapter.isEnabled() &&
                bluetoothAdapter.getState() == BluetoothAdapter.STATE_ON) {
            color = getResources().getColor(R.color.enabled);
        }
        findViewById(R.id.checkBoxStatusBluetooth).setBackgroundColor(color);

        NfcManager manager = (NfcManager) getSystemService(Context.NFC_SERVICE);
        NfcAdapter adapter = manager.getDefaultAdapter();
        color = getResources().getColor(R.color.disabled);
        if (adapter != null && adapter.isEnabled()) {
            color = getResources().getColor(R.color.enabled);
        }
        findViewById(R.id.textViewStatusNFC).setBackgroundColor(color);


//        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, CHANNEL_ID)
//                .setSmallIcon(R.drawable.baseline_phone_disabled_black_48)
//                .setContentTitle(getApplicationContext().getString(R.string.stay_offline))
//                .setContentText(getApplicationContext().getString(R.string.stay_offline))
//                .setPriority(NotificationCompat.PRIORITY_DEFAULT);

    }


    /**
     * Gets the state of Airplane Mode.
     *
     * @param context
     * @return true if enabled.
     */
    public static boolean isAirplaneModeOn(Context context) {
        return Settings.Global.getInt(context.getContentResolver(),
                Settings.Global.AIRPLANE_MODE_ON, 0) != 0;
    }
}